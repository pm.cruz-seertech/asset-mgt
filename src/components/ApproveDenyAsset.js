import React, { Component } from 'react';
import { Table, Button, Column} from 're-bulma';

class Home extends Component {
  render() {
    return (
      <Table isBordered isStriped isNarrow>

        <thead>
            <h1>Approve/Deny Request</h1>
            <tr>
                <th>Asset Name</th>
                <th>Asset Type</th>
                <th>Model</th>
                <th>Date Requested</th>
                <th>Requested By</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Macbook Pro</td>
                <td>Laptop</td>
                <td>model-1a</td>
                <td>March 26, 2018</td>
                <td>Paolo Miguel Cruz</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Approve</Button>
                        <Button isColor='success' isOutlined>Deny</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Macbook Pro Retina</td>
                <td>Laptop</td>
                <td>model-1b</td>
                <td>March 28, 2018</td>
                <td>Anaya Tingson</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Approve</Button>
                        <Button isColor='success' isOutlined>Deny</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Iphone X</td>
                <td>Mobile Phone</td>
                <td>model-2a</td>
                <td>April 1, 2018</td>
                <td>Ralph Bautista</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Approve</Button>
                        <Button isColor='success' isOutlined>Deny</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Iphone 6</td>
                <td>Mobile Phone</td>
                <td>model-2b</td>
                <td>April 2, 2018</td>
                <td>Jonathan Roque</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Approve</Button>
                        <Button isColor='success' isOutlined>Deny</Button>
                    </Column>
                </td>
            </tr>
        </tbody>
    </Table>
    );
  }
}

export default Home;