import React, { Component } from 'react';
import { Menu, MenuLabel, MenuList, MenuLink} from 're-bulma';

class Drawer extends Component {
  render() {
    return (
        <Menu>
            <MenuLabel>Asset Management</MenuLabel>
                <MenuList>
                    <li><MenuLink>Asset List</MenuLink></li>
                    <li><MenuLink>Request List</MenuLink></li>
                </MenuList>
        </Menu>
    );
  }
}
export default Drawer;