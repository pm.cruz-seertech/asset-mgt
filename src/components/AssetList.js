import React, { Component } from 'react';
import { Menu, MenuLabel, MenuList, MenuLink, Table, Button, Column} from 're-bulma';

class AssetList extends Component {
  render() {
    return (
        <Table isBordered isStriped isNarrow>

        <thead>
            <h1>Asset Lists</h1>
            <tr>
                <th>Asset Name</th>
                <th>Asset Type</th>
                <th>Model</th>
                <th>Date Purchased</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Macbook Pro</td>
                <td>Laptop</td>
                <td>dakhfskahqr1sda</td>
                <td>July 2014</td>
                <td>Available</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Request</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Macbook Pro Retina</td>
                <td>Laptop</td>
                <td>aieruqou123</td>
                <td>July 2015</td>
                <td>In-Use</td>
                <td>
                    <Column hasTextAlign='centered' isActive = 'false'>
                        <Button isColor='success' isOutlined>Request</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Iphone X</td>
                <td>Mobile Phone</td>
                <td>dkfjhgwerkhfds</td>
                <td>December 2017</td>
                <td>In-Use</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Request</Button>
                    </Column>
                </td>
            </tr>
            <tr>
                <td>Iphone 6</td>
                <td>Mobile Phone</td>
                <td>porou131</td>
                <td>Feb 2018</td>
                <td>Available</td>
                <td>
                    <Column hasTextAlign='centered'>
                        <Button isColor='success' isOutlined>Request</Button>
                    </Column>
                </td>
            </tr>
        </tbody>
    </Table>
    );
  }
}

export default AssetList;