import React, { Component } from 'react';
import { Box, Button, Column, FormHorizontal, Group, Input, Label, Section} from 're-bulma';

class App extends Component {
  render() {
    return (
      <Section>
        <h1>Create an Account</h1>
        <Column>
        <Box>
          <Label>Username:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="Username..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
            help={{
              color: 'isSuccess',
              text: 'This username is available',
            }}
          />
          <Label>Full Name:</Label>
          <Input
            color="isSuccess"
            type="text"
            placeholder="Full Name..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
          />
          <Label>Password</Label>
          <Input
            color="isSuccess"
            type="password"
            placeholder="Password..."
            defaultValue=""
            icon="fa fa-check"
            hasIconRight
            help={{
              color: 'isSuccess',
              text: 'This username is available',
            }}
          />
          <Button
            Primary>Submit
          </Button>
        </Box>
      </Column>
      </Section>
    );
  }
}

export default App;
