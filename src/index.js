import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/AssetList';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
